package main

import (
	"context"
	vesselPb "bitbucket.org/tiluvy/shippy-service-vessel/proto/vessel"
	pb "bitbucket.org/tiluvy/shippy-service-consignment/proto/consignment"
	"log"
)

type repository interface {
	Create(*pb.Consignment) error
	GetAll() ([]*pb.Consignment, error)
}

type handler struct {
	repo repository
	vesselClient vesselPb.VesselServiceClient
}

func (s *handler) CreateConsignment(ctx context.Context, req *pb.Consignment, res *pb.Response) error {
	log.Println("CreateConsignment")
	log.Println(req)
	vesselResponse, err := s.vesselClient.FindAvailable(context.Background(), &vesselPb.Specification{
		MaxWeight: req.Weight,
		Capacity: int32(len(req.Containers)),
	})
	if err != nil {
		return err
	}

	req.VesselId = vesselResponse.Vessel.Id
	// Save our consignment
	if err = s.repo.Create(req); err != nil {
		return err
	}

	res.Created = true
	res.Consignment = req
	return nil
}

func (s *handler) GetConsignments(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	log.Println("GetConsignments")
	consignments,_ := s.repo.GetAll()
	res.Consignments = consignments
	return nil
}

