// shippy-service-consignment/main.go
package main

import (
	"context"
	"github.com/micro/go-micro"
	vesselPb "bitbucket.org/tiluvy/shippy-service-vessel/proto/vessel"
	pb "bitbucket.org/tiluvy/shippy-service-consignment/proto/consignment"
	"fmt"
	"os"
	"errors"
	"log"
	"github.com/micro/go-micro/server"
	microClient "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	userService"bitbucket.org/tiluvy/shippy-service-user/proto/user"
)

const (
	port = ":50051"
	defaultHost = "mongodb://localhost:27017"
)

func AuthWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, resp interface{}) error {
		meta, ok := metadata.FromContext(ctx)
		if !ok {
			return errors.New("no auth meta-data found in request")
		}
		log.Println(meta)

		// Note this is now uppercase (not entirely sure why this is...)
		token := meta["Token"]
		log.Println("Authenticating with token: ", token)

		// Auth here
		authClient := userService.NewUserServiceClient("shippy.service.user", microClient.DefaultClient)
		_, err := authClient.ValidateToken(context.Background(), &userService.Token{
			Token: token,
		})
		if err != nil {
			return err
		}
		err = fn(ctx, req, resp)
		return err
	}
}

func main() {
	srv := micro.NewService(
		// This name must match the package name given in your protobuf definition
		micro.Name("shippy.service.consignment"),
		micro.Version("latest"),
		// Our auth middleware
		micro.WrapHandler(AuthWrapper),
	)
	// Init will parse the command line flags.
	srv.Init()

	uri := os.Getenv("DB_HOST")
	if uri == ""{
		uri = defaultHost
	}
	client, err := CreateClient(uri)
	if err != nil {
		log.Panic(err)
	}
	defer client.Disconnect(context.TODO())

	consignmentCollection := client.Database("shippy").Collection("consignments")

	repository := &MongoRepository{consignmentCollection}
	vesselClient := vesselPb.NewVesselServiceClient("shippy.service.vessel", srv.Client())

	h := &handler{repository, vesselClient}

	// Register handler
	pb.RegisterShippingServiceHandler(srv.Server(),h)

	// Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
