package main

import (
	pb "bitbucket.org/tiluvy/shippy-service-consignment/proto/consignment"
	"go.mongodb.org/mongo-driver/mongo"
	"context"
	"go.mongodb.org/mongo-driver/bson"
)

type MongoRepository struct {
	collection *mongo.Collection
}

func (repository *MongoRepository) GetAll() ([]*pb.Consignment, error) {
	cur, err := repository.collection.Find(context.Background(), bson.D{}, nil)
	var consignments []*pb.Consignment
	for cur.Next(context.Background()) {
		var consignment *pb.Consignment
		if err := cur.Decode(&consignment); err != nil {
			return nil, err
		}
		consignments = append(consignments, consignment)
	}
	return consignments, err
}

func (repository *MongoRepository) Create (consignment *pb.Consignment) error  {
	_, err := repository.collection.InsertOne(context.Background(), consignment)
	return err
}


func (repository *MongoRepository) Close() {

}