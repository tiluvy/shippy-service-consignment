module bitbucket.org/tiluvy/shippy-service-consignment

go 1.12

require (
	bitbucket.org/tiluvy/shippy-service-user v0.0.0-00010101000000-000000000000
	bitbucket.org/tiluvy/shippy-service-vessel v0.0.0-20191007100628-9d57ba2e344d
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/armon/circbuf v0.0.0-20190214190532-5111143e8da2 // indirect
	github.com/armon/go-metrics v0.0.0-20190430140413-ec5e00d3c878 // indirect
	github.com/armon/go-radix v1.0.0 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/google/pprof v0.0.0-20190723021845-34ac40c74b70 // indirect
	github.com/hashicorp/go-immutable-radix v1.1.0 // indirect
	github.com/hashicorp/go-msgpack v0.5.5 // indirect
	github.com/hashicorp/go-version v1.2.0 // indirect
	github.com/hashicorp/mdns v1.0.1 // indirect
	github.com/hashicorp/serf v0.8.3 // indirect
	github.com/micro/go-micro v1.18.0
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/posener/complete v1.2.1 // indirect
	go.mongodb.org/mongo-driver v1.2.1
	golang.org/x/mobile v0.0.0-20190806162312-597adff16ade // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
)

replace bitbucket.org/tiluvy/shippy-service-vessel => ../shippy-service-vessel

replace bitbucket.org/tiluvy/shippy-service-user => ../shippy-service-user
